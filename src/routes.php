<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

include 'location.php';

define('PASSWORD_SALT', 'xlasljh5sef4');


function validateToken($token, $db){
    $stmt = $db->prepare('SELECT * FROM auth_users WHERE token = :token');
    $stmt->bindValue(':token', $token);
    $stmt->execute();
    $auth = $stmt->fetch();
    # pokud token existuje v DB vrati true
    return !empty($auth['token']);
}

$app->get('/', function (Request $request, Response $response, $args) {
    // Render index view
    return $this->view->render($response, 'index.latte');
})->setName('index');


$app->post('/test', function (Request $request, Response $response, $args) {
    //read POST data
    $input = $request->getParsedBody();

    //log
    $this->logger->info('Your name: ' . $input['person']);

    return $response->withHeader('Location', $this->router->pathFor('index'));
})->setName('redir');



/*  Nacteni formulare pro registraci*/
$app->get('/register', function(Request $request, Response $response, $args){
    $tplVars = [
    'formData' => [
        'email' => '',
        'password' => ''
      ],
      'title' => 'Register'
    ];
    return $this->view->render($response, 'register.latte', $tplVars);
})->setName('register');

/* Registrace uzivatele*/
$app->post('/register', function(Request $request, Response $response, $args){
    $formData = $request->getParsedBody();
    #overeni noveho uzivatele
    $stmt = $this->db->prepare('SELECT * FROM auth_users WHERE email = :email');
    $stmt->bindParam(':email', $formData['email']);
    $stmt->execute();
    $user = $stmt->fetch();

    if (! empty($user['email']) ) {
        $tplVars['message'] = 'Sorry this email address is already used!';
        return $this->view->render($response, 'register.latte', $tplVars);

    } else {
        try {
        $this->db->beginTransaction();
        $password = md5(PASSWORD_SALT . $formData['password']);
        $token = bin2hex(openssl_random_pseudo_bytes(20));

        $stmt = $this->db->prepare('INSERT INTO auth_users (email, password, token) VALUES (:email, :password, :token)');
        $stmt->bindValue(':email', $formData['email']);
        $stmt->bindValue(':password', $password);
        $stmt->bindValue(':token', $token);
        $stmt->execute();
        $this->db->commit();    
        setcookie('token', $token, 0);

        return $response->withHeader('Location', $this->router->pathFor('persons'));
        } catch (PDOexception $e) {
            $this->logger->error($e);
            $this->db->rollback();
            return $this->view->render($response, 'register.latte', $tplVars);
        }
    }
});

    /* ===  LOGIN === */

$app->get('/login', function(Request $request, Response $response, $args){
    $tplVars = [
    'formData' => [
        'email' => '',
        'password' => ''
      ],
      'title' => 'Log In'
    ];

    return $this->view->render($response, 'register.latte', $tplVars);
})->setName('login');

$app->post('/login', function(Request $request, Response $response, $args){
    $formData = $request->getParsedBody();

    $tplVars = [
    'formData' => $formData,
    'title' => 'Log In'
    ];

    #overeni existence uzivatele

    $stmt = $this->db->prepare('SELECT * FROM auth_users WHERE email = :email AND password = :password');
    $stmt->bindValue('email', $formData['email']);
    $stmt->bindValue('password', md5(PASSWORD_SALT . $formData['password']));
    $stmt->execute();
    $user = $stmt->fetch();

    if (empty($user['email'])) {
        $tplVars['message'] = 'Email or password is incorrect!';
        return $this->view->render($response, 'register.latte', $tplVars);
    } else {
        $token = bin2hex(openssl_random_pseudo_bytes(20));
        $stmt = $this->db->prepare('UPDATE auth_users SET token = :token WHERE id_users = :id_users');
        $stmt->bindValue(':token', $token);
        $stmt->bindValue(':id_users', $user['id_users']);
        $stmt->execute();

        setcookie('token', $token, 0);
        return $response->withHeader('Location', $this->router->pathFor('persons'));
    }
});

    /* === LOGOUT === */

$app->get('/logout', function(Request $request, Response $response, $args){
    setcookie('token', '', time() - 3600);
    
    return $response->withHeader('Location', $this->router->pathFor('index'));
})->setName('logout');


/* ----- Vsechny routy budou dostupne po prihlaseni (s platnym tokenem) ------- */

$app->group('/auth', function() use($app){


    /* vypis vsech osob z DB */
    $app->get('/persons', function (Request $request, Response $response, $args) {
    	$params = $request->getQueryParams();
        if (empty($params ['limit'])) {
            $params ['limit'] = 10;
        }

        if (empty($params ['page'])) {
            $params ['page'] = 0;
        }

        $stmt = $this->db->query('SELECT count(*) pocet FROM person');
        $total_pages = $stmt->fetch()['pocet'] / $params['limit'];


        $stmt = $this->db->prepare('SELECT id_person, first_name, last_name, nickname, height FROM person ORDER BY first_name LIMIT :limit OFFSET :offset'); #databazovy objekt, kurzor
        // * - lze nahradit za first_name, last_name, nickname, height 
    	// $stmt = statement, $tplVars = template variables
        $stmt->bindValue(':limit', $params['limit']);
        $stmt->bindValue(':offset', $params['page'] * $params['limit']);
        $stmt->execute();

    	$tplVars = [
            'persons_list' => $stmt->fetchall(),
            'total_pages' => $total_pages,
            'page' => $params['page'],
            'limit' => $params['limit']
        ]; # [['id_person' => 1,'first_name' => 'pepa'...], ['id_person' => ...]]
    	return $this->view->render($response, 'persons.latte', $tplVars);
    })->setName('persons');


    /*vyhledavani osob*/

    $app->get('/person/search', function(Request $request, Response $response, $args){
        //vrati asociativni pole
        $queryParams = $request->getQueryParams();

        if (! empty($queryParams) && ! empty($queryParams['query'])){
            $stmt = $this->db->prepare('SELECT first_name, last_name, nickname, height FROM person WHERE lower (first_name) = lower (:fname) OR lower (last_name) = lower (:lname) ORDER BY first_name');
            // : vytvorena promenna, neni v default databazi
            // lower - case insensitive
            $stmt->bindParam(':fname', $queryParams['query']);
            $stmt->bindParam(':lname', $queryParams['query']);
            $stmt->execute();
            $tplVars['persons_list'] = $stmt->fetchall();
            return $this->view->render($response, 'persons.latte', $tplVars);
        }
    })->setName('search');

    /*pridani formulare pro novou osobu*/
    $app->get('/person', function(Request $request, Response $response, $args){
        $tplVars['header'] = 'New person';
        $tplVars['formData'] = [
            'first_name' => '',
            'last_name' => '',
            'nickname' => '',
            'gender' => '',
            'height' => '',
            'birth_day' => '',
            'street_name' => '',
            'street_number' => '',
            'city' => '',
            'zip' => ''
          ];

        return $this->view->render($response, 'person-form.latte', $tplVars);
    })->setName('newPerson');

    /*post nova metoda*/
    $app->post('/person', function(Request $request, Response $response, $args){
        $formData = $request->getParsedBody();
        $tplVars = [];

        if ( empty($formData['first_name']) || empty($formData['last_name']) || empty($formData['nickname']) ){
            $tplVars['message'] = 'Vyplňte požadované pole';
            } else { 
                  try {
                    
                    if ( !empty($formData['street_name']) || !empty($formData['street_number']) || ! empty($formData['zip'])  ) {
                        $id_location = newLocation($this, $formData);
                    } else {
                        $id_location = null;
                    }

                    $stmt = $this->db->prepare('INSERT INTO person (first_name, last_name, nickname, gender, height, birth_day, id_location) VALUES (:first_name, :last_name, :nickname, :gender, :height, :birth_day, :id_location)');

                    $stmt->bindParam(':first_name', $formData['first_name']);
                    $stmt->bindParam(':last_name', $formData['last_name']);
                    $stmt->bindParam(':nickname', $formData['nickname']);
                    $stmt->bindValue(':gender', empty($formData['gender']) ? null : $formData['gender']); 
                    /*podminka if empty{
                        return null
                    } else {
                        return $formData [];
                    }
                    */
                    $stmt->bindValue(':height', empty($formData['height'])  ? null : $formData['height']); 
                    $stmt->bindValue(':birth_day', empty($formData['birth_day'])  ? null : $formData['birth_day']);
                    $stmt->bindValue(':id_location', $id_location ? $id_location : null); 
     
                    $stmt->execute();
                    } catch (PODexception $e) {
                        $tplVars['message'] = 'Error, sorry jako';
                        $tplVars ['formData'] = $formData;
                        $this->logger->error($e->getMessage());              
                    }        
            }

            $tplVars['header'] = 'New person';
            return $this->view->render($response, 'person-form.latte', $tplVars);

    });


    /* Update osoby */
    $app->get('/person/{id_person}/edit', function(Request $request, Response $response, $args){
        #kontrola autorizace uzivatele

        if (! empty($args['id_person'])) {
            $stmt = $this->db->prepare('SELECT first_name, last_name, nickname, gender, height, birth_day, street_name, street_number, city, zip FROM person LEFT JOIN location USING (id_location) WHERE id_person = :id_person');
            $stmt->bindValue(':id_person', $args['id_person']);
            $stmt->execute();
            $tplVars['formData'] = $stmt->fetch();

            if ( empty($tplVars['formData']) ) {
                exit(['Person not found']);
            } else {
                $tplVars ['header'] = 'Update person';
                return $this->view->render($response, 'person-form.latte', $tplVars);
            }
        }
    })->setName('updatePerson');


    /*update osoby post*/
    $app->post('/person/{id_person}/edit', function(Request $request, Response $response, $args){
        $formData = $request->getParsedBody();

        if ( empty($formData['first_name']) || empty($formData['last_name']) || empty($formData['nickname']) ){
            $tplVars['message'] = 'Vyplňte požadované pole';
            } else {
                try {
                    #kontrola vyplneni casti adresy
                    if ( !empty($formData['street_name']) || !empty($formData['street_number']) || ! empty($formData['zip'])  ) {
                        $stmt = $this->db->prepare('SELECT id_location FROM person WHERE id_person = :id_person');
                        $stmt->bindValue(':id_person', $args['id_person']);
                        $stmt->execute();
                        $id_location = $stmt->fetch()['id_location'];
                        
                        if ($id_location) {
                            #osoba ma adresu (id_location IS NOT NULL), editujeme
                            editLocation($this, $formData, $id_location);
                        } else {
                            #osoba nema adresu(id_location IS NULL), vkladame
                            $id_location = newLocation($this, $formData);
                        }
                    }

                    $stmt = $this->db->prepare('UPDATE person SET first_name = :first_name,
                                                                    last_name = :last_name,
                                                                    nickname = :nickname,
                                                                    birth_day = :birth_day,
                                                                    gender = :gender,
                                                                    height = :height,
                                                                    id_location = :id_location
                                                                    WHERE id_person = :id_person' );

                    $stmt->bindValue(':first_name', $formData['first_name']);
                    $stmt->bindValue(':last_name', $formData['last_name']);
                    $stmt->bindValue(':nickname', $formData['nickname']);
                    $stmt->bindValue(':gender', empty($formData['gender']) ? null : $formData['gender']);
                    $stmt->bindValue(':height', empty($formData['height'])  ? null : $formData['height']); 
                    $stmt->bindValue(':birth_day', empty($formData['birth_day'])  ? null : $formData['birth_day']);
                    $stmt->bindValue(':id_location', $id_location ? $id_location : null);
                    $stmt->bindValue(':id_person', $args['id_person']);
                    $stmt->execute();
                    $tplVars['message'] = 'Person successfully updated';
                    
                } catch (PDOexception $e) {
                    $tplVars['message'] = 'Error, sorry jako';
                    $this->logger->error($e->getMessage());
                }
            }
            $tplVars ['formData'] = $formData;
            $tplVars['header'] = 'Edit person';
            return $this->view->render($response, 'person-form.latte', $tplVars);
    });

    /* DELETE osoby*/

    $app->get('/person/{id_person}/delete', function(Request $request, Response $response, $args){
        if (!empty($args['id_person'])) {
            try {
              $stmt=$this->db->prepare('DELETE FROM contact WHERE id_person = :id_person' );
              $stmt->bindValue(':id_person', $args['id_person']);
              $stmt->execute();

              $stmt=$this->db->prepare('DELETE FROM person_meeting WHERE id_person = :id_person' );
              $stmt->bindValue(':id_person', $args['id_person']);
              $stmt->execute();

              $stmt=$this->db->prepare('DELETE FROM relation WHERE id_person1 = :id_person OR id_person2 = :id_person' );
              $stmt->bindValue(':id_person', $args['id_person']);
              $stmt->bindValue(':id_person', $args['id_person']);
              $stmt->execute();

              $stmt=$this->db->prepare('DELETE FROM person WHERE id_person = :id_person' );
              $stmt->bindValue(':id_person', $args['id_person']);
              $stmt->execute();


            } catch (PDOexception $e) {
                $this->logger->error($e->getMessage());
            }
        } else {
            exit('id_person is missing');
        }

        return $response->withHeader('Location', $this->router->pathFor('persons'));

    })->setName('deletePerson');



    #------------------------------
  
    /* -- PERSON INFO -- */
    $app->get('/person/info', function (Request $request, Response $response, $args) {
        $id_person = $request->getQueryParam('id_person');
        if (!empty($id_person)) {
            try {
                $stmt = $this->db->prepare('SELECT * FROM person LEFT JOIN location
                                                USING (id_location) WHERE id_person = :id_person');
                $stmt->bindValue(':id_person', $id_person);
                $stmt->execute();
                $tplVars['person'] = $stmt->fetch();

                
                $stmt = $this->db->prepare('SELECT * FROM person INNER JOIN contact
                                            USING (id_person) INNER JOIN contact_type
                                            USING (id_contact_type) WHERE id_person = :id_person');
                $stmt->bindValue(':id_person', $id_person);
                $stmt->execute();
                $tplVars['contact'] = $stmt->fetchall();                


                $stmt = $this->db->prepare('SELECT DISTINCT id_person, person.first_name, person.last_name, name, id_relation, friend_first_name, friend_last_name,description FROM person 
                                        INNER JOIN relation ON (person.id_person = relation.id_person1)
                                        INNER JOIN relation_type USING (id_relation_type)
                                        INNER JOIN (SELECT id_person AS friend_id, first_name AS friend_first_name, last_name AS friend_last_name FROM person) AS friend ON (id_person2 = friend.friend_id)
                                        WHERE id_person = :id_person');
                $stmt->bindValue(':id_person', $id_person);
                $stmt->execute();
                $tplVars['relation'] = $stmt->fetchall();
                         
            } catch (PDOexception $e) {
                $this->logger->error($e->getMessage());
                exit('Something is wrong');
            }
        } else {
            exit('This person doesnt exit!');
        }

        return $this->view->render($response, 'infoPerson.latte', $tplVars);

    })->setName('infoPerson');

    /* -- RELATION LIST -- */
    $app->get('/relation', function (Request $request, Response $response, $args) {
        
        $params = $request->getQueryParams();
        if (empty($params ['limit'])) {
            $params ['limit'] = 11;
        }

        if (empty($params ['page'])) {
            $params ['page'] = 0;
        }

        $stmt = $this->db->query('SELECT count(*) id_relation FROM relation');
        $total_pages = $stmt->fetch()['id_relation'] / $params['limit'];
        
        $stmt = $this->db->prepare("SELECT DISTINCT CONCAT (p1.first_name, ' ', p1.last_name) as person_name,
                                                    CONCAT (p2.first_name, ' ', p2.last_name) as friend_name,
                                        relation_type.name, relation.description, relation.id_relation 
                                    FROM relation
                                    JOIN person as p1 ON relation.id_person1 = p1.id_person  
                                    JOIN person as p2 ON relation.id_person2 = p2.id_person
                                    JOIN relation_type ON relation.id_relation_type = relation_type.id_relation_type
                                    ORDER BY person_name LIMIT :limit OFFSET :offset");
       
        $stmt->bindValue(':limit', $params['limit']);
        $stmt->bindValue(':offset', $params['page'] * $params['limit']);
        $stmt->execute();
                
        $tplVars = [
            'relation_list' => $stmt->fetchall(),
            'total_pages' => $total_pages,
            'page' => $params['page'],
            'limit' => $params['limit']
        ];
                
        return $this->view->render($response, 'relation.latte', $tplVars);
    })->setName('relation');


    /* -- FORM RELATION -- */

    $app->get('/relationAdd', function (Request $request, Response $response, $args) {
        $stmt = $this->db->query("SELECT CONCAT(first_name, ' ',last_name) as person_name, id_person FROM person ORDER BY person_name");
        $tplVars['header'] = 'Add new relation';
        $tplVars['persons'] = $stmt->fetchall();

        return $this->view->render($response, 'relationAdd.latte', $tplVars);
    })->setName('relationAdd');

    /* -- POST RELATION -- */
    $app->post('/relationAdd', function (Request $request, Response $response, $args) {
        $formData = $request->getParsedBody();
        
        if (empty($formData['id_person1']) || empty($formData['id_person2']) || empty($formData['id_relation_type'])){
            $tplVars['message'] = 'You have to fill required fields';

        } elseif (($formData['id_person1']) == ($formData['id_person2'])){
            $tplVars['message'] = 'I know you would like to, but you cannot have relation with yourself!';
        } else {
            try{

                $stmt = $this->db->prepare( 'INSERT INTO relation ( id_person1, id_person2, id_relation_type, description) VALUES ( :id_person1, :id_person2, :id_relation_type, :description)' );
                $stmt->bindValue(":id_person1", $formData['id_person1']);
                $stmt->bindValue(":id_person2", $formData['id_person2']);
                $stmt->bindValue(":id_relation_type", $formData['id_relation_type']);
                $stmt->bindValue(":description", $formData['description']);
                $stmt->execute();

                $tplVars['message'] = 'Relation was succesfully created';

            } catch(PDOException $e) {
                $tplVars['message'] = $e->getMessage();
            }

        }
        $tplVars['formData'] = $formData;
        return $this->view->render($response,'relationAdd.latte', $tplVars);

    });


    /* -- DELETE RELATION -- */

    $app->post('/relation/deleteRelation', function(Request $request, Response $response, $args){
        $id_relation = $request->getQueryParam('id_relation');
        if (!empty($id_relation) ) {
            try {
              $stmt = $this->db->prepare('DELETE FROM relation WHERE id_relation = :id_relation');
              $stmt->bindValue(':id_relation', $id_relation);
              $stmt->execute();                          

            } catch (PDOexception $e) {
                $this->logger->error($e->getMessage());
                exit('Error occured - delete');
            }
        } else {
            exit('id_relation is missing');
        } 
        
         return $response->withHeader('Location', $this->router->pathFor('relation'));

    })->setName('deleteRelation');


    /* -- CONTACT FORM --*/
    
    $app->get('/person/info/contactAdd', function (Request $request, Response $response, $args) {
        $stmt = $this->db->prepare("SELECT id_contact_type, name FROM contact_type ORDER BY id_contact_type");
        $stmt->execute();
        $tplVars['contact'] = $stmt->fetchall();
        
        // $stmt = $this->db->query('SELECT id_person, first_name, last_name FROM person WHERE id_person = :id_person');
        $stmt = $this->db->query('SELECT id_person, first_name, last_name FROM person');
        //$stmt->bindValue(':id_person', $tplVars['id_person']);
        $stmt->execute();
        $tplVars['person'] = $stmt->fetch();
    
        $tplVars['header'] = 'New contact';

        return $this->view->render($response, 'contactAdd.latte', $tplVars);
    })->setName('newContact');


    /* -- NEW CONTACT -- */

    $app->post('/person/info/contactAdd', function (Request $request, Response $response, $args) {
    $formData = $request->getParsedBody();
    $id_person = $request->getQueryParam('id_person');

    if ( empty($id_person) OR empty($formData['id_contact_type']) OR empty($formData['contact']) ) {
        $tplVars['message'] = 'Missing ID!';
    } else {
        try {
        $stmt = $this->db->prepare("INSERT INTO contact (id_person, id_contact_type, contact) VALUES (:id_person, :id_contact_type, :contact)");
        $stmt->bindValue(":id_person", $id_person);
        $stmt->bindValue(":id_contact_type", $formData['id_contact_type']);
        $stmt->bindValue(":contact", $formData['contact']);
        $stmt->execute();
        $tplVars['message'] = "Contact successfully added!";
        
        } catch (PDOException $e) {
            $this->logger->error($e->getMessage());
            exit('error add contact');
        
        }
    }
    return $response->withHeader('Location', $this->router->pathFor('infoPerson').'?id_person='.$id_person);
    })->setName('newContact');
        
    /* -- DELETE CONTACT -- */
    # chyba ve vykresleni po odeslani, odebere kontakt, ale hodi chybu z r.406-info person

    $app->post('/person/info/deleteCont', function (Request $request, Response $response, $args) {
    $id_contact = $request->getQueryParam('id_contact');
    $id_person = $request->getQueryParam('id_person');

    if (!empty($id_contact)){
        try {
        $stmt = $this->db->prepare('DELETE FROM contact WHERE id_contact = :id_contact');
        $stmt->bindValue(':id_contact', $id_contact); 
        $stmt->execute();
                
        } catch (PDOexception $e) {
            $this->logger->error($e->getMessage());
            exit('error delete contact');
        }

    } else {
        exit('id contact is missing');
    }
    return $response->withHeader('Location', $this->router->pathFor('infoPerson').'?id_person='.$id_person);
    })->setName('deleteContact');

    

    /*  ----- MEETING ----- */

    /* -- MEETING LIST -- */

    $app->get('/person/meeting', function (Request $request, Response $response, $args) {
        $stmt = $this->db->query("SELECT id_meeting, start, description,
                                    CONCAT(location.street_name, ' ', location.street_number, ', ',location.city) as address
                                    FROM meeting JOIN location 
                                    ON meeting.id_location = location.id_location ORDER BY start DESC");
        $tplVars['meeting_list'] = $stmt->fetchall();

        return $this->view->render($response,'meeting.latte', $tplVars);

    })->setName('meetings');

    /* -- INFO MEETING -- */ 

    $app->get('/meeting/info', function (Request $request, Response $response, $args) {
    $id_meeting = $request->getQueryParam('id_meeting');
    try {
        $stmt = $this->db->prepare('SELECT start, duration, description, street_name, street_number, city, zip, country, id_meeting FROM meeting INNER JOIN location USING (id_location)
                                    WHERE id_meeting = :id_meeting');
        $stmt->bindValue(':id_meeting', $id_meeting);
        $stmt->execute();
        $tplVars['meeting'] = $stmt->fetch();

        $stmt = $this->db->prepare('SELECT DISTINCT * FROM meeting INNER JOIN person_meeting USING (id_meeting) 
                                    INNER JOIN person USING (id_person)
                                    WHERE id_meeting = :id_meeting');
        $stmt->bindValue(':id_meeting', $id_meeting);
        $stmt->execute();
        $tplVars['person'] = $stmt->fetchall();

    } catch (PDOexception $e) {
        $this->logger->error($e->getMessage());
        exit('error occured');
    }
    return $this->view->render($response, 'infoMeeting.latte', $tplVars);
    })->setName("infoMeeting");

    
    /* -- DELETE MEETING -- */

    // nejede, chyba r. 643

    $app->post('/meeting/deleteMeeting', function (Request $request, Response $response, $args) {
        $id_meeting = $request->getQueryParam('id_meeting');
        
        try {
                $stmt = $this->db->prepare('DELETE FROM meeting WHERE id_meeting = :id_meeting');
                $stmt->bindValue(':id_meeting', $id_meeting);
                $stmt->execute();

            } catch (PDOException $e) {
                $this->logger->error($e->getMessage());
                exit('Error - delete meeting');
            }      

    return $response->withHeader('Location', $this->router->pathFor('meetings'));
    })->setName('deleteMeeting');
    

    /* -- FORM NEW MEETING -- */

    $app->get('/meeting', function (Request $request, Response $response, $args) {
    $stmt = $this->db->query('SELECT location.id_location, city, street_name, street_number, zip, country
                                 FROM location WHERE city IS NOT NULL ORDER BY city');
    $tplVars['location_list'] = $stmt->fetchall();
    $tplVars['formData'] = [
        'description' => '',
        'duration' => '',
        'start' => '',
        'name' => ''
        ];

    return $this->view->render($response, 'newMeeting.latte', $tplVars);
    })->setName('newMeeting');

    /* -- POST NEW MEETING -- */

    $app->post('/meeting', function (Request $request, Response $response, $args) {
        $formData = $request->getParsedBody();
        
        try {
            $stmt = $this->db->prepare("INSERT INTO meeting (start, description, duration, id_location) VALUES (:start, :description, :duration, :id_location)");
            $stmt->bindValue(':start', $formData['start']);
            $stmt->bindValue(':description', $formData['description']);
            $stmt->bindValue(':duration', $formData['duration']);
            $stmt->bindValue(':id_location', $formData['id_location']);
            $stmt->execute();
            $stmt = $this->db->prepare("SELECT id_meeting FROM meeting WHERE (id_location=:id_location AND start=:start AND duration=:duration AND description=:description)");
            $stmt->bindValue(':start', $formData['start']);
            $stmt->bindValue(':description', $formData['description']);
            $stmt->bindValue(':duration', $formData['duration']);
            $stmt->bindValue(':id_location', $formData['id_location']);
            $stmt->execute();
            $tplVars["id_meeting"] = $stmt->fetch();
            
        } catch (PDOexception $e) {
            $tplVars['message'] = 'Error occured';
            $this->logger->error($e->getMessage());
        }

        return $response->withHeader('Location', $this->router->pathFor('meetings'));
    });


    /* -- JOIN PERSON MEETING -- */

    $app->get('/meeting/join', function (Request $request, Response $response, $args) {
        $params = $request->getQueryParams();
            try {
                $stmt = $this->db->prepare('SELECT DISTINCT person.id_person, first_name, last_name, nickname FROM person LEFT JOIN person_meeting ON person.id_person = person_meeting.id_person WHERE person_meeting.id_meeting != :id_meeting OR person_meeting IS NULL ORDER BY first_name');
                $stmt->bindValue(":id_meeting", $params['id_meeting']);
                $stmt->execute();
                $tplVars['person_list'] = $stmt->fetchAll(); 
                return $this->view->render($response, 'joinMeeting.latte', $tplVars);
            }catch (PDOException $e){
                $this->logger->error($e->getMessage());
                exit('error of person list');
            }
    })->setName('joinMeeting');

    $app->post('/meeting/join', function (Request $request, Response $response, $args) {
        $id_meeting = $request->getQueryParam('id_meeting');
        $formData = $request->getParsedBody();
        $tplVars = [];

        if (empty($formData['id_person']) OR empty($id_meeting)) {
            $tplVars['message'] = "error";
        }
        else {  
        try {
            $stmt = $this->db->prepare("INSERT INTO person_meeting (id_person, id_meeting) VALUES (:id_person, :id_meeting)");
            $stmt->bindValue(":id_person", $formData['id_person']);
            $stmt->bindValue(":id_meeting", $id_meeting);
            $stmt->execute();
            

        } catch (PDOException $exception) {
            $this->logger->error($exception->getMessage());
            $tplVars['message'] = "error add relation";
        }
        $tplVars['formData'] = $formData;
        $tplVars['message'] = "Person successfully added";
        }
        return $this->view->render($response, 'joinMeeting.latte', $tplVars);
       
    })->setName('joinMeeting');


    /* -- DELETE PERSON FROM MEETING -- */
        // odebere osobu, ale hodi chybu (z radku 623), po refresh osoba chybi

    $app->post('/meeting/info/deletePerson', function (Request $request, Response $response, $args) {
        $formData['id_person'] = $request->getQueryParam('id_person'); 
        if (!empty($formData['id_person'])){
            try{
            $stmt = $this->db->prepare('DELETE FROM person_meeting WHERE id_person = :id_person');
            $stmt->bindValue(':id_person', $formData['id_person']); 
            $stmt->execute();
            } catch (PDOexception $e) {
                $this->logger->error($e->getMessage());
                exit('error delete');
            }

        } else {
            exit('id person is missing');
        }
    
    return $response->withHeader('Location', $this->router->pathFor('infoMeeting').'?id_meeting='.$id_meeting);

    })->setName('deleteMeeting_Person');

    
#konec group
})->add(function($request, $response, $next) {
    # vynuceni autentizace
    if (empty($_COOKIE['token'])  || validateToken($token, $this->db) ) {
        return $response->withHeader('Location', $this->router->pathFor('login'));

    } else {
        return $next($request, $response);
    }
    
});

